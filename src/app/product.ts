export class Product {
    id: number;
    prod_name: string;
    prod_quant: number;
    prod_price: number;
    availability: boolean;
    picture: string;
    updated_at: Date;
  }