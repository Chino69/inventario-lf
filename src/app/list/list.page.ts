import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import * as firebase from 'Firebase';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isNgTemplate } from '@angular/compiler';
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
  });
 console.log(returnArr)
  return returnArr;
};@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss'],
})

export class ListPage implements OnInit {
  details = [];
  info = {}
  infoForm: FormGroup;
  ref = firebase.database().ref('details/');
  constructor( public router: Router, private navCtrl: NavController, public loadingController: LoadingController, private formBuilder: FormBuilder, private route: ActivatedRoute, public alertController: AlertController) {{firebase.database().ref('details/'+this.route.snapshot.paramMap.get('id')).on('value', snapshot => {
    if(snapshot.val()){
      console.log(snapshot.val())
    
  
      this.info = snapshot.val()
    
      console.log(this.info)}
  }); }
    this.ref.on('value', resp => {
      this.details = [];
      this.details = snapshotToArray(resp);
    });
    this.infoForm = this.formBuilder.group({
      'prod_name' : ['', Validators.required],
      'prod_price' : ['', Validators.required],
      'prod_quant' : ['', Validators.required]
    });
  }
  
  addQuant(key){
    console.log(key)
 
    let quant; 
    firebase.database().ref('details/'+ key).once('value').then(snapshot => {
      console.log(snapshot.val())
    quant = snapshot.val().prod_quant
      console.log(quant)
  }).then(()=> {
    
    quant ++;
    firebase.database().ref('details/'+ key).update({prod_quant: quant})

  });


  }
  subtQuant(key){
    console.log(key)
    let avail;
    let quant; 
    firebase.database().ref('details/'+ key).once('value').then(snapshot => {
      console.log(snapshot.val())
    quant = snapshot.val().prod_quant
    avail = snapshot.val().availability
      console.log(quant)
  }).then(()=> {
    if(quant===0){

    }else{quant --;
      if(quant === 0 && !avail){
        this.delete(key)
      }else{
        firebase.database().ref('details/'+ key).update({prod_quant: quant})  }
      }});}
  

      async delete(key) {
        const alert = await this.alertController.create({
          header: 'Confirm!',
          message: 'Agreeing will delete this product, are you sure?',
          buttons: [
            {
              text: 'No',
              role: 'cancel',
              cssClass: 'secondary',
              handler: (blah) => {
                console.log('cancel');
              }
            }, {
              text: 'Yes',
              handler: () => {
                firebase.database().ref('details/'+key).remove(); 
              }
            }
          ]
        });
      
        await alert.present();
      }
    
      
    

 
  
  
  
  ngOnInit(){
  }
}
  
