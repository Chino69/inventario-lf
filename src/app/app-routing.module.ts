import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
//import { AuthGuard } from './services/user/auth.guard';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'details/:id', loadChildren: './details/details.module#DetailsPageModule', /*canActivate: [AuthGuard]*/ },
  { path: 'edit/:id', loadChildren: './edit/edit.module#EditPageModule', /*canActivate: [AuthGuard]*/ },
  { path: 'list', loadChildren: './list/list.module#ListPageModule', /*canActivate: [AuthGuard]*/ },
  { path: 'add', loadChildren: './add/add.module#AddPageModule', /*canActivate: [AuthGuard]*/},
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule', /*canActivate: [AuthGuard]*/ },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
