import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { ActivatedRoute, Router  } from '@angular/router';
import { NavController } from '@ionic/angular';
import {  FormBuilder, FormGroup, Validators } from '@angular/forms';
export const snapshotToObject = snapshot => {
  let item = snapshot.val();

  return item;
}
@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  ref = firebase.database().ref('details/');
  info = {};
  infoForm: FormGroup;
  constructor(private route: ActivatedRoute, private navCtrl: NavController,
    public router: Router,
    private formBuilder: FormBuilder) {{firebase.database().ref('details/'+this.route.snapshot.paramMap.get('id')).on('value', snapshot => {
      if(snapshot.val()){
        console.log(snapshot.val())
      
    
        this.info = snapshot.val()
      
        console.log(this.info)}
    }); }
    
      this.infoForm = this.formBuilder.group({
        'prod_name' : ['', Validators.required],
        'prod_price' : ['', Validators.required],
        'prod_quant' : ['', Validators.required],
        'availability': [''],
        'picture': ['']
      });

      //this.getInfo(this.route.snapshot.paramMap.get('key'));
     }
    /*getInfo(key) {
      firebase.database().ref('details/'+key).on('value', resp => {
        let info = snapshotToObject(resp);
        this.infoForm.controls['prod_name'].setValue(info.prod_name);
        this.infoForm.controls['prod_price'].setValue(info.prod_price);
        this.infoForm.controls['prod_desc'].setValue(info.prod_desc);
      });
    }*/
    
    updateInfo() {
      console.log(this.route.snapshot.paramMap.get('id'))
      let newInfo = firebase.database().ref('details/'+this.route.snapshot.paramMap.get('id')).update(this.infoForm.value);
      this.router.navigate(['/details/'+this.route.snapshot.paramMap.get('id')]);
    }
  ngOnInit() {
  }

}
