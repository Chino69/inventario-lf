import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { ActivatedRoute, Router  } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})

export class DetailsPage implements OnInit {
  info = {};
  constructor(private route: ActivatedRoute, private navCtrl: NavController,
    public router: Router) {firebase.database().ref('details/'+this.route.snapshot.paramMap.get('id')).on('value', snapshot => {
      if(snapshot.val()){
      console.log(snapshot.val())
      
    
        this.info = snapshot.val()
      
      console.log(this.info)}
    }); }
    goToEdit(){
      let key = this.route.snapshot.paramMap.get('id');
      this.navCtrl.navigateForward(['/edit/'+key]);
    }
    
   
    goHome(){
      this.navCtrl.navigateBack(['app/tabs/home']); 
    }
    deleteInfo(){
      let key = this.route.snapshot.paramMap.get('id');
      this.router.navigate(['/list']);      
      firebase.database().ref('details/'+key).remove();

    }        
            
       
  ngOnInit() {
  }
}
