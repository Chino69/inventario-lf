import { Component, OnInit } from '@angular/core';
import * as firebase from 'Firebase';
import { ActivatedRoute, Router  } from '@angular/router';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from '../firebase.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { async } from '@angular/core/testing';
@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  ref = firebase.database().ref('details/');
  infoForm: FormGroup;
  constructor(private route: ActivatedRoute, private imagePicker: ImagePicker, private firebaseService: FirebaseService, private webview: WebView,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public router: Router,
    private formBuilder: FormBuilder) {
      this.infoForm = this.formBuilder.group({
        'prod_name' : [null, Validators.required],
        'prod_price' : [null, Validators.required],
        'prod_quant' : [null, Validators.required],
        'availability': [0, Validators.required],
        'picture': [null]
      });
     }
     async presentLoading(loading) {
      return await loading.present();
    }
     async openImagePicker(){
      await this.imagePicker.hasReadPermission()
      .then(async (result) => {
        if(result == false){
          // no callbacks required as                    this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if(result == true){
        console.log("Ya tiene permisos Eso!");
        //  await this.imagePicker.getPictures({
        //     maximumImagesCount: 1
        //   }).then(
        //    async (results) => {
        //       for (var i = 0; i < results.length; i++) {
        //       await  this.uploadImageToFirebase(results[i]);
        //       }
        //     }, (err) => console.log(err)
        //   );
        }
      }, (err) => {
        console.log(err);
      });
    }
    async uploadImageToFirebase(image){
      const loading = await this.loadingCtrl.create({
        message: 'Please wait...'
      });
      const toast = await this.toastCtrl.create({
        message: 'Image was updated successfully',
        duration: 3000
      });
      this.presentLoading(loading);
      let image_src = this.webview.convertFileSrc(image);
      let randomId = Math.random().toString(36).substr(2, 5);
  
      //uploads img to firebase storage
      this.firebaseService.uploadImage(image_src, randomId)
      .then(photoURL => {
        image = photoURL;
        loading.dismiss();
        toast.present();
        firebase.database().ref('details/'+ firebase.database().ref('details/').key).update({picture: photoURL})
        
        
      }, err =>{
        console.log(err);
      })
    }

     saveInfo() {
     firebase.database().ref('details/').push(this.infoForm.value)
      
    
   
      this.router.navigate(['/list']);
      this.infoForm.setValue({prod_name:"", prod_price:"", prod_quant:"",availability:0});
    
    }   
    
  ngOnInit() {
  }

}